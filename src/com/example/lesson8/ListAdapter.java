package com.example.lesson8;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListAdapter extends ArrayAdapter<String>{

	private LayoutInflater inflater;
	private ArrayList<String> data;
	public ListAdapter(Context context, ArrayList<String> data) {
		super(context, R.layout.listitem, data);
        inflater = LayoutInflater.from(context);
        this.data = data;

	}

	public View getView(int position, View convertView, ViewGroup parent)
	{
		 View view = convertView;
		 if (view ==null)
		 {
			 view   = inflater.inflate(R.layout.listitem, null);

		 }
		 
		 String item = data.get(position);
		 if (item != null)
		 {
			 TextView textview = (TextView) view.findViewById(R.id.textview);
			 textview.setText(item);			 
			 
		 }
		return view;
	}	
	
	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}
}

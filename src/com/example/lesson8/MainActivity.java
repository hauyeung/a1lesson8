package com.example.lesson8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.example.lesson8.ListAdapter;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;


public class MainActivity extends Activity {
	ListView listview;
	ListAdapter adapter;
	ArrayList<String> data = new ArrayList<String>();
	
	MenuItem reversesort;
	String newval;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		data.add("alpha");
		data.add("beta");
		data.add("gamma");
		data.add("test");
		setContentView(R.layout.activity_main);
		adapter = new ListAdapter(this,data);
		adapter.sort(new Comparator<String>() {
	            @Override
	            public int compare(String arg0, String arg1) {
	                return arg0.compareTo(arg1);
	            }
	        });
		listview = (ListView) findViewById(R.id.list);
		listview.setAdapter(adapter);
		registerForContextMenu(listview);
		reversesort = (MenuItem) findViewById(R.id.reversesort);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.optionsmenu, menu);		
		return true;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.contextmenu, menu);
	}
	
	public void add()
	{		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText input = new EditText(MainActivity.this);
		builder.setMessage("Please enter text");
		builder.setView(input);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){					
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				data.add(input.getText().toString());
				
				
				
			}});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub						
			}
			
		});
		builder.show();
		adapter = new ListAdapter(this,data);
		adapter.sort(new Comparator<String>() {
            @Override
            public int compare(String arg0, String arg1) {
                return arg0.compareTo(arg1);
            }
        });
		listview = (ListView) findViewById(R.id.list);
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
	
	public void reversesort()
	{		
		Collections.sort(data,new Comparator<String>() {
	            @Override
	            public int compare(String arg0, String arg1) {
	                return arg1.compareTo(arg0);
	            }
	        });
			adapter = new ListAdapter(this,data);
			listview = (ListView) findViewById(R.id.list);
			listview.setAdapter(adapter);
			adapter.notifyDataSetChanged();
	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {		
		    switch (item.getItemId()) {
				case R.id.add:
					add();
					return true;
				case R.id.reversesort:
					reversesort();
					return true;
				default:
					return super.onOptionsItemSelected(item);
		    }
			
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	    
	        case R.id.edit:
	            edit(adapter.getItem(info.position));
	            return true;
	        case R.id.delete:
	            adapter.remove(adapter.getItem(info.position));
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
	
	public void edit(final String s)
	{	
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final EditText input = new EditText(MainActivity.this);
		builder.setMessage("Please enter text");
		builder.setView(input);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){					
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				data.set(data.indexOf(s),input.getText().toString());
				
				
				
			}});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub						
			}
			
		});
		builder.show();
		adapter = new ListAdapter(this,data);
		adapter.sort(new Comparator<String>() {
            @Override
            public int compare(String arg0, String arg1) {
                return arg0.compareTo(arg1);
            }
        });
		listview = (ListView) findViewById(R.id.list);
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}

	
	

	
	

}
